/* global Template Skills Members*/
Template.ProfileTemplate.onCreated(function() {
	Meteor.subscribe('members')
})

Template.ProfileTemplate.onRendered(function(){
	$('#attachSkillForm').validate()
})

Template.ProfileTemplate.helpers({
	memberSkills(code) {
		return Skills.find({code: code}).fetch()
	},
	allskills() {
		return Skills.find({}, {sort: {name: 1}}).fetch()
	}
})

Template.ProfileTemplate.events({
	'submit .attach-skill': function(e, t){
		e.preventDefault()
		let route = Router.current()
		let skill = t.find('#skills').value
		let id = t.find('#id').value
		let member = Members.findOne({_id: route.params._id})
		let skills = member.skills
		let	position = -1
		if(!skills){
		} else {
			position = skills.findIndex(input => input == skill)
		}
		if(position == -1)
		{
			Meteor.call('attachMemberSkill', id, skill)
		} else {
			alert('This Member Already Has That Skill')
		}
	},
	'click .skill-badge': function(e, t){
		route = Router.current()
		let skill = this
		let id = t.find('#id').value
		let member = Members.findOne({_id: route.params._id})
		let skills = member.skills
		let position = skills.findIndex(input => input == skill)
		Meteor.call('removeMemberSkill', member, skills, position)
	}
})
