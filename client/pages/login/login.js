Template.loginForm.onRendered(function(){
   if($("#email").val() != "" || $("#email").val() != null){
         $("#email").parent().addClass('active');
   }
   if($("#password").val() != "" || $("#password").val() != null){
         $("#password").parent().addClass('active');
   }
});

Template.loginTemplate.events({})

Template.loginForm.events({
	'submit .login-form': function(e, t){
		e.preventDefault()
		let email = t.find('#email').value
		let password = t.find('#password').value
		Meteor.loginWithPassword(email, password, function(err){
			if(err){
				console.log(err)
			}
		})
	},
	'click .cancel': function(e, t){
		t.find('#password').value = ""
		t.find('#email').value = ""
	},
	'click div.form-group input': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).focus();
	},
	'click div.form-group legend': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).parent().find('input').focus();
	},
	'focus div.form-group input': function(event){
		$(event.target).parent().addClass('active');
	}
})
