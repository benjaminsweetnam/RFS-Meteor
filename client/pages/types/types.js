/* global Template Meteor Types*/

Template.TypesTemplate.onCreated(function() {
	Meteor.subscribe('types')
})

Template.TypesTemplate.helpers({
	types() {
		let types = Types.find({}, {sort: {name: 1}})
		return types
	}
})

Template.TypeTemplate.events({
	'click .delete': function(){
		Types.remove(this._id)
	}
})
Template.insertTypeForm.onRendered( function(){
	$('#insertTypeForm').validate()
})

Template.insertTypeForm.events({
	'submit .new-type': function(e, t){
		e.preventDefault()
		let name = t.find('#name').value
		if(name == '')
		{
			alert('Please Enter Type')
		}
		let type = Types.findOne({name: name})
		if(!type)
		{
			Meteor.call('insertType', name)
		}else{
			alert('Cannot Enter Two Types With The Same Name')
		}

		t.find('#name').value = ''
	},
	'click div.form-group input': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).focus();
	},
	'click div.form-group legend': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).parent().find('input').focus();
	},
	'focus div.form-group input': function(event){
		$(event.target).parent().addClass('active');
	}
})
