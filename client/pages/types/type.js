/* global Template Types Skills*/
Template.TypeSkillTemplate.onCreated(function() {

})

Template.TypeSkillTemplate.helpers({
	typeSkills(code) {
		return Skills.find({code: code}).fetch()
	},
	allskills() {
		return Skills.find({}, {sort: {name: 1}}).fetch()
	}
})

Template.TypeSkillTemplate.onRendered( function(){
	$('#attchTypeSkillForm').validate()
})

Template.TypeSkillTemplate.events({
	'submit .attach-type-skill': function(e, t){
		e.preventDefault()
		let id = t.find('#id').value
		let skill = t.find('#typeSkills').value
		let type = Types.findOne({_id: id}, {elemMatch:{required: skill}})
		let index = type.required.findIndex(input => input == skill)
		if(index === -1){
			Meteor.call('attachTypeSkill', id, skill)
		} else {
			alert('This Skill Is Already In The Call Type')
		}
	},
	'click .skill': function(e, t){
		e.preventDefault()
		let skill = this
		let id = t.find('#id').value
		let type = Types.findOne({_id: id})
		let required = type.required
		let position = required.findIndex(input => input == skill)
		Meteor.call('removeTypeSkill', id, skill, required, position)
	}
})
