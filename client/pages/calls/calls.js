/* eslint no-console: 0 */
/* global Session*/

Template.CallsTemplate.onCreated(function() {
	Meteor.subscribe('members')
	Meteor.subscribe('skills')
	Meteor.subscribe('types')
	Session.setDefault('skip', 0)
	Deps.autorun(function(){
		Meteor.subscribe('calls')
	})
})

Template.CallsTemplate.helpers({
	// calls: calls
	calls() {
		var calls = Calls.find({}, {sort: {createdAt: -1}, limit: 4, skip: Session.get('skip')})
		return calls
	},
	prevPageClass(){
		if(Session.get('prevButton') == true){
			return 'disabled'
		} else {
			return null
		}
	},
	nextPageClass(){
		if(Session.get('nextButton') == true){
			return 'disabled'
		} else {
			return null
		}
	}
})

Template.CallsTemplate.events({
	'click #nextPage': function(){
 		let page = Session.get('skip')
		let calls = Calls.find({}).count()
		if((page+4)>=calls){
			Session.set('nextButton', true)
		} else {
			Session.set('prevButton', false)
			Session.set('skip', page+4)
		}
	},
	'click #prevPage': function(){
		let page = Session.get('skip')
		if(page>0){
			Session.set('nextButton', false)
			Session.set('skip', page-4)
		} else {
			Session.set('prevButton', true)
		}
		}
	})

Template.insertCallForm.helpers({
	types() {
		return Types.find({})
	},
	members() {
		return Members.find({})
	}
})

Template.Call.helpers({
	callOfficer(memberId) {
		member = Members.find({_id: memberId}, {limit: 1}).fetch()[0]
		return member.first + ' ' + member.last
	},
	formatDate(date){
		return moment(date).format('llll')
	}
})
Template.insertCallForm.onRendered( function(){
	$('#insertCallForm').validate({})
})
Template.insertCallForm.events({
	'submit .new-call': function(e, t){
		e.preventDefault()
		var members = Members.find({})
		var type = t.find('#type').value
		var message = 'CHERRYBROOK RFS - ' + type + ': ' + t.find('#message').value
		var officer = t.find('#officer').value
		members.forEach(function(member){
			var smsOptions =	{
				to: member.mobile,
				message: message + ' - Reply YES & minutes till arrival or NO',
			}
			Meteor.call('sendSMS', smsOptions, function(err, res){
				if (err) {
					alert(err)
					return
				}
			})
		})
		var callOptions = {
			message: message,
			type: type,
			officer: officer
		}
		Meteor.call('insertCall', callOptions)
		t.find('#message').value = ''
	}
})

Template.CallsTemplate.events({
	'click .delete': function(e) {
		e.preventDefault()
		Meteor.call('removeCall', 	this._id)
	},
	'click form#insertCallForm fieldset input': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).focus();
	},
	'click form#insertCallForm fieldset label': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).parent().find('input').focus();
	}
})
