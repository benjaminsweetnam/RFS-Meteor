Template.UsersTemplate.onCreated(function usersOnCreated() {
	Meteor.subscribe('users')
})

Template.insertUserForm.onRendered(function(){
   $('#insertUserForm').validate();
   if($("#email").val() != "" || $("#email").val() != null){
         $("#email").parent().addClass('active');
   }
   if($("#password").val() != "" || $("#password").val() != null){
         $("#password").parent().addClass('active');
   }
})

Template.UsersTemplate.helpers({
	users() {
		return Meteor.users.find()
	}
})

Template.insertUserForm.events({
	'submit .new-user': function(e, t){
		e.preventDefault()
		var email = t.find('#email').value
		var password = t.find('#password').value
		Meteor.call('newUser', email, password);
		t.find('#email').value = ''
		t.find('#password').value = ''
	}
})

Template.UsersTemplate.events({
	'click .delete': function(e){
		e.preventDefault()
		var user = Meteor.users.findOne({'emails.address': this.address})
		Meteor.call('removeUser', user)
	}
})
