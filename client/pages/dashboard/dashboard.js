/* global Calls Template Members Session moment*/

Template.Dashboard.helpers({
	lastCall() {
		let call = Calls.find({}, {sort: {createdAt: -1}, limit: 1}).fetch()[0]
		call.createdAt = moment(call.createdAt).format('llll')
		Session.set('call', call._id)
		return call
	},
	required() {
		let call = Calls.find({_id: Session.get('call')}, {fields: {'type': 1}}).fetch()[0]
		let types = Types.findOne({name: call.type})
		types.required.sort()
		return types
	},
	stillrequired(passed){
		var skills = Session.get('skills')
		if(skills.indexOf(passed) == -1){
			return null
		} else {
			return "skill-hide"
		}
	},
	officer(memberId){
		let member = Members.findOne({_id: memberId})
		return 'Officer: ' + member.first + ' ' + member.last
	}
})
Template.LastCall.helpers({
response() {
	let callId = Session.get('call')
	let responses = Response.find({callId: callId})
	let memberIds = responses.map(r => r.memberId)
	memberIds = memberIds.filter((v,i)=> memberIds.indexOf(v) == i)
	let members = Members.find({_id: {$in: memberIds}}).fetch()
	let skills = [].concat.apply([], members.map(r => r.skills))
	if(skills){
		Session.set('skills', skills)
	}
	responses = memberIds.map(i => Response.find({callId: callId, memberId: i}, {sort: {callId: 1}, limit: 1}).fetch())
	responses = [].concat.apply([], responses)
	let front = responses.filter(a => a.time == null )
	responses = responses.filter(a => a.time != null)
	responses = responses.sort((a, b) => a.time - b.time)
	responses = responses.concat(front)
	return responses
},
// response() {
// 	let callId = Session.get('call')
// 	let response = Response.find({callId: callId}).fetch()
// 	// response = response.sort((a, b) => b.time - a.time)
// 	response = response.sort((a, b) => b.ceatedAt - a.createdAt)
// 	response = response.map(r => r.memberId)
// 	response = response.filter((v,i)=> response.indexOf(v) == i)
// 	return response
// },
mems(memberId){
	let callid = Session.get('call')
	let members = Members.find({_id: memberId}).fetch()
	let response = Response.find({callId: callid, memberId: memberId}, {sort: {createdAt: 1}, limit: 1}).fetch()
	return members
},
members() {
	let callid = Session.get('call')
	let responses = Response.find({callId: callid}).map(r => r.memberId)
	let members = Members.find({_id: {$in: responses}}).fetch()
	var test = [].concat.apply([], members.map(r => r.skills))
	if(test){
		Session.set('skills', test)
	}
	return members
},
sms(memberId) {
	let callid = Session.get('call')
	let time = Response.find({callId: callid, memberId: memberId}, {sort: {createdAt: -1}, limit: 1}).fetch()
	return time
},
	formatDate() {
		var data = new ReactiveDict
		data.set("now", new Date)
		setTimeout(function () {
		  data.set("now", new Date)
		}, 1000)
		var currentTime = function () {
		  data.get("now")
	}
	now = moment(data.get("now"))
	if(this.time != null){
		let arrival = moment(this.createdAt).add(this.time, 'm')
	  arrival = arrival.diff(now, 'm')
		arrival += 1
		if(arrival <= 0){
			return "Arrived"
		} else {
			return arrival + " mins"
		}
	} else {
			return "Not Attending"
	}
	}
})
