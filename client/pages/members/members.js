Template.MembersTemplate.onCreated(function() {
	Meteor.subscribe('members')
})

Template.MembersTemplate.helpers({
	members() {
		let members = Members.find({}, {sort: {first: 1}})
		return members
	}
})

Template.MembersTemplate.events({
	'click .delete': function(e){
		e.preventDefault()
		Meteor.call('removeMember', this._id)
	}
})

Template.insertMemberForm.onRendered( function(){
	$('#insertMemberForm').validate()
})

Template.insertMemberForm.events({
	'submit .new-member': function(e, t){
		e.preventDefault()
		var first = t.find('#first').value
		var last = t.find('#last').value
		var mobile = t.find('#mobile').value
		if(mobile.startsWith('04')){
			mobile = mobile.replace('04', '+614')
		}
		let member = Members.findOne({first: first, last: last})
		let mobilecheck = Members.findOne({mobile: mobile})

		if(first == '', last == '', mobile == ''){
			alert('Fields Empty')
		} else if (member){
			alert('Member Already Exists')
		} else if (mobilecheck) {
			alert('Mobile Already Exists')
		} else {
			Meteor.call('addMember', first, last, mobile)
		}
		t.find('#first').value = ''
		t.find('#last').value = ''
		t.find('#mobile').value = ''
	},
	'click div.form-group input': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).focus();
	},
	'click div.form-group legend': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).parent().find('input').focus();
	},
	'focus div.form-group input': function(event){
		$(event.target).parent().addClass('active');
	}
})
