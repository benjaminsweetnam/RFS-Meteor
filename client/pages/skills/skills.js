/* global Template Meteor Skills*/

Template.SkillsTemplate.onCreated(function() {
	Meteor.subscribe('skills')
})

Template.SkillsTemplate.helpers({
	skills() {
		let skills = Skills.find({}, {sort: {name: 1}}).fetch()
		return skills
	}
})

Template.SkillsTemplate.events({
	'click .remove-skill': function() {
		Skills.remove(this._id)
	}
})

Template.insertSkillForm.onRendered( function(){
	$('#insertSkillForm').validate()
})

Template.insertSkillForm.events({
	'submit .new-skill': function(e, t) {
		e.preventDefault()
		let name = t.find('#skill-name').value
		let code = t.find('#skill-code').value
		let nameCheck = Skills.findOne({name: name})
		let codeCheck = Skills.findOne({code: code})
		if(nameCheck) {
			alert("Skill Name Already Used!")
		} else if(codeCheck) {
			alert("Skill Code Already Used!")
		}
		Meteor.call('addSkill', code, name)
		t.find('#skill-name').value = ""
		t.find('#skill-code').value = ""
	},
	'click div.form-group input': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).focus();
	},
	'click div.form-group legend': function(event){
		$(event.target).parent().addClass('active');
		$(event.target).parent().find('input').focus();
	},
	'focus div.form-group input': function(event){
		$(event.target).parent().addClass('active');
	}
})
