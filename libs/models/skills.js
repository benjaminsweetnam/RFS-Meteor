/* global Mongo, Skills, SimpleSchema, Meteor */

Skills = new Mongo.Collection('skills')

let Schema = {}
// Remove For Production
Skills.allow({
	insert: function() {
		return true
	},
	remove: function() {
		return true
	},
	update: function() {
		return true
	}
})
//
Schema.Skills = new SimpleSchema({
	name: {
		type: String,
		label: 'Skill Name'
	},
	code: {
		type: String,
		label: 'Skill Code'
	},
	createdAt: {
		type: Date,
		autoValue: function() {
			if (this.isInsert) {
				return new Date()
			} else if(this.isUpsert) {
				return {$setOnInsert: new Date()}
			} else {
				this.unset()
			}
		}
	}
})

Skills.attachSchema(Schema.Skills)
