/* global Mongo, Members, SimpleSchema*/

Members = new Mongo.Collection('members')

Members.helpers({
	responses() {
		return Response.find({memberId: this._id})
	}
})
Meteor.users.allow({
	remove: function(){
		return true;
	}
})
Members.allow({
	insert: function() {
		return true
	},
	remove: function() {
		return true
	},
	update: function() {
		return true
	}
})

let Schemas = {}

Schemas.Members = new SimpleSchema({
	first: {
		type: String,
		label: 'First Name'
	},
	last: {
		type: String,
		label: 'Last Name'
	},
	mobile: {
		type: String,
		label: 'Mobile Number'
	},
	skills: {
		type: [String],
		optional: true
	}
})

Members.attachSchema(Schemas.Members)
