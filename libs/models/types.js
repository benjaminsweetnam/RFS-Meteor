/* global Mongo, Types, SimpleSchema Meteor*/

Types = new Mongo.Collection('types')

let Schema = {}
// Remove For Production
Types.allow({
	insert: function() {
		return true
	},
	remove: function() {
		return true
	},
	update: function() {
		return true
	}
})
//
Schema.Types = new SimpleSchema({
	name: {
		type: String,
		label: 'Type Name'
	},
	required: {
		type: [String],
		optional: true
	}
})

Types.attachSchema(Schema.Types)
