/* global Mongo, Calls, SimpleSchema, Types */

Calls = new Mongo.Collection('calls')

Calls.helpers({
	responses() {
		return Response.find({ callId: this._id })
	},
	requirements() {
		return Types.find({ name: this.type })
	}
})

var Schema = {}

Calls.allow({
	insert: function() {
		return true
	},
	remove: function() {
		return true
	},
	update: function() {
		return true
	}
})

Schema.Calls = new SimpleSchema({
	message: {
		type: String,
		label: 'Message'
	},
	type: {
		type: String,
		label: 'Call Type'
	},
	officer: {
		type: String,
		label: 'Officer'
	},
	createdAt: {
		type: Date,
		autoValue: function() {
			if (this.isInsert) {
				return new Date()
			} else if(this.isUpsert) {
				return {$setOnInsert: new Date()}
			} else {
				this.unset()
			}
		}
	}
})

Calls.attachSchema(Schema.Calls)
