/* global Mongo, Response, SimpleSchema, Calls, Members, Meteor */

Response = new Mongo.Collection('response')

Response.helpers({
	call() {
		return Calls.findOne({ _id: this.callId })
	},
	member() {
		return Members.find({ _id: this.memberId })
	}
})

Response.allow({
	insert: function() {
		return true
	},
	remove: function() {
		return true
	},
	update: function() {
		return true
	}
})

let Schemas = {}

Schemas.Response = new SimpleSchema({
	time: {
		type: Number,
		label: 'Time',
		optional: true
	},
	memberId: {
		type: String,
		label: 'User Id'
	},
	callId: {
		type: String,
		label: 'Call Id'
	},
	createdAt: {
		type: Date,
		autoValue: function() {
			if (this.isInsert) {
				return new Date()
			} else if(this.isUpsert) {
				return {$setOnInsert: new Date()}
			} else {
				this.unset()
			}
		}
	}
})

Response.attachSchema(Schemas.Response)
