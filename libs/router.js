/* global Router Meteor Members Calls Types*/
/* eslint no-console: 1 */
Router.onBeforeAction(function() {
	if (!Meteor.user() && this.ready()){
		if (Meteor.loggingIn()){
		} else {
			return this.redirect('/login')
		}
	}
	this.next()
}, {except: ['api', 'login']})

Router.configure({
	layoutTemplate: 'Main'
})



Router.route('/', {
	waitOn: function(){
		Meteor.subscribe('calls'),
		Meteor.subscribe('response'),
		Meteor.subscribe('members'),
		Meteor.subscribe('types')
	},
	action: function(){
		this.render('Dashboard')
	}
})

Router.route('/fullpage', {
	waitOn: function(){
		Meteor.subscribe('calls'),
		Meteor.subscribe('response'),
		Meteor.subscribe('members'),
		Meteor.subscribe('types')
	},
	action: function(){
		this.layout('FullLayout')
		this.render('Dashboard')
	}
})

Router.route('/users', {
	waitOn: function() {
		Meteor.subscribe('users')
	},
	action: function() {
		this.render('UsersTemplate')
	}
})

Router.route('/members', {
	waitOn: function () {
		Meteor.subscribe('members')
	},
	action: function() {
		this.render('MembersTemplate')
	}
})

Router.route('/member/:_id', {
	waitOn: function() {
		Meteor.subscribe('member', this.params._id),
		Meteor.subscribe('skills')
	},
	action: function() {
		this.render('ProfileTemplate', {
			data: function() {
				return Members.findOne({_id: this.params._id})
			}
		})
	}
})

Router.route('/calls', {
	waitOn: function(){
		Meteor.subscribe('calls')
	},
	action: function(){
		this.render('CallsTemplate')
	}
})

Router.route('/types', {
	waitOn: function(){
		Meteor.subscribe('types')
	},
	action: function(){
		this.render('TypesTemplate')
	}
})

Router.route('/types/:_id', {
	waitOn: function(){
		Meteor.subscribe('type', this.params._id),
		Meteor.subscribe('skills')
	},
	action: function(){
		this.render('TypeSkillTemplate', {
			data: function(){
				return Types.findOne({_id: this.params._id})
			}
		})
	}
})

Router.route('/skills', {
	waitOn: function(){
		Meteor.subscribe('skills')
	},
	action: function(){
		this.render('SkillsTemplate')
	}
})

Router.route('/login', {
	action: function(){
		if(!Meteor.user()){
			this.layout('loginTemplate')
		} else if(Meteor.user()){
			this.redirect('/')
		}
	}
})
// Router.onBeforeAction(Iron.Router.bodyParser.json(), {except: ['api'], where: 'server'})
// Router.route('/api', {where: 'server'})
// 	.post(function()	{
//
//
// 		this.response.writeHead(200, {'Content-Type': 'text/html'})
// 		this.response.end('<html><body>Works</body></html>')
// 	})

Router.map(function() {
	this.route('api', {
		path: '/api',
		where: 'server',
		action: function(){
			var requestMethod = this.request.method
			var requestData = this.request.body
			var body = JSON.stringify(requestData.Body)
			var from = JSON.stringify(requestData.From)
			var method = JSON.stringify(requestMethod)
			var body = body.replace(/"/g, "")
			var body = body.split(" ")
			var trigger = body[0].toLowerCase()
			from = from.replace(/"/gi,	 "")
			var rest = body[1]
			for(var i = 2;i < body.length;i++){
				rest = rest + ' ' + body[i]
			}
			var message = 'CHERRYBROOK RFS - ' + rest
			if( trigger == 'firecall') {
				var officer = Members.find({mobile: from}, {limit: 1}).fetch()
				Calls.insert({
					message: message,
					type: body[1],
					officer: officer[0]._id
				})
				var members = Members.find({})
				members.forEach(function(member){
					let smsOptions = {
						to: member.mobile,
						message: 'CHERRYBROOK RFS - Firecall: ' + rest + ' - Reply YES & minutes till arrival or NO'
					}
					Meteor.call('sendSMS', smsOptions, function(err, res){
						if (err) {
							console.error(err)
						}
					})
				})
			} else if(trigger == 'yes') {
					var member = Members.find({mobile: from}, {limit: 1}).fetch()[0]
					var call = Calls.find({}, {sort: {createdAt: -1, limit: 1}}).fetch()[0]
					Response.insert({
						time: rest,
						memberId: member._id,
						callId: call._id
					})
					let officer = Members.findOne({_id: call.officer})
					let smsOptions = {
						to: officer.mobile,
						message: member.first + ' ' + member.last + ' will be attending station in ' + rest + ' mins'
					}
					Meteor.call('sendSMS', smsOptions, function(err, res){
						if(err) {
							console.error(err)
						}
					})
				} else if(trigger == 'no') {
					var member = Members.find({mobile: from}, {limit: 1}).fetch()[0]
					var call = Calls.find({}, {sort: {createdAt: -1, limit: 1}}).fetch()[0]
					Response.insert({
						memberId: member._id,
						callId: call._id
					})
					let officer = Members.findOne({_id: call.officer})
					let smsOptions = {
						to: officer.mobile,
						message: member.first + ' ' + member.last + ' is not attending station'
					}
					Meteor.call('sendSMS', smsOptions, function(err, res){
						if(err){
							console.error(err)
						}
					})
				}
			this.response.writeHead(200, {'Content-Type': 'text/xml'})
			this.response.end(`
				<?xml version="1.0" encoding="UTF-8"?>
				<Response>
				</Response>
			`)
		}
	})
})
