Meteor.publish('calls', function(){
	if(this.userId){
		return Calls.find({})
	}
})
Meteor.publish('members', function(){
	if(this.userId){
		return Members.find({})
	}
})
Meteor.publish('member', function(memberId){
	if(this.userId){
		return Members.find({_id: memberId}, {limit: 1})
	}
})
Meteor.publish('users', function(){
	if(this.userId){
		return Meteor.users.find({}, {fields: {'emails': 1}})
	}
})
Meteor.publish('response', function(){
	if(this.userId){
		return Response.find({})
	}
})
Meteor.publish('skills', function(){
	if(this.userId){
		return Skills.find({})
	}
})
Meteor.publish('types', function(){
	if(this.userId){
		return Types.find({})
	}
})
Meteor.publish('type', function(typeId){
	if(this.userId){
		return Types.find({_id: typeId}, {limit: 1})
	}
})
SMS = new Mongo.Collection('sms')

var twilioClient = new Twilio({
	from: Meteor.settings.TWILIO.FROM,
	sid: Meteor.settings.TWILIO.SID,
	token: Meteor.settings.TWILIO.TOKEN
})
Meteor.methods({
	'userLogin': function(email, password){
		Meteor.loginWithPassword(email, password, function(err){
			if(err){
				Meteor.error(err)
			}else{
				return true
			}
		})

	},
	'removeCall': function(id) {
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				Calls.remove(id)
			} catch (err) {
				throw new Meteor.error(err)
			}
		}
	},
	'addSkill': function(code, name){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				var result = Skills.insert({
					code: code,
					name: name
				})
			} catch(err) {
				throw new Meteor.error(err)
			}
			return result
		}
	},
	'attachMemberSkill': function(id, code){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				var result = Members.update({_id: id}, {$push: {skills: code}})
			} catch (err) {
				throw new Meteor.error(err)
			}
			return result
		}
	},
	'removeMemberSkill': function(member, skills, position){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				var result = Members.update({_id: member._id}, {$pull: {skills: skills[position]}})
			} catch (err) {
				throw new Meteor.error(err)
			}
			return result
		}
	},
	'addMember': function(first, last, mobile){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				var result = Members.insert({first: first, last: last, mobile: mobile})
			} catch (err) {
				throw new Meteor.error(err)
			}
			return result
		}
	},
	'removeMember': function(id){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				var result = Members.remove(id)
			} catch (err) {
				throw new Meteor.error(err)
			}
			return result
		}
	},
	'insertType': function(name){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				var result = Types.insert({name: name, required: []})
			} catch (err) {
				throw new Meteor.error(err)
			}
			return result
		}
	},
	'removeType': function(id){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				return result = Types.remove(id)
			} catch (err) {
				throw new Meteor.error(err)
			}
			var result
		}
	},
	'attachTypeSkill': function(id, skill){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				var result = Types.update({_id: id}, {$push: {required: skill}})
			} catch (err) {
				throw new Meteor.error(err)
			}
			return result
		}
	},
	'removeTypeSkill': function(id, skill, required, position){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
			try {
				var result = Types.update({_id: id}, {$pull: {required: required[position]}})
			} catch (err) {
				throw new Meteor.error(err)
			}
			return result
		}
	},
	'newUser': function(email, password){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} else {
		try {
				Accounts.createUser({email: email, password: password})
				return false
			} catch (err) {
				throw new Meteor.Error(err)
			}
		}
	},
	'removeUser': function(user){
		if(!this.userId){
			throw new Meteor.Error('Not Authorized!')
		} try {
			var result = Meteor.users.remove(user._id)
		} catch (err) {
			throw new Meteor.Error(err)
		}
		return result
	},
	'sendSMS': function (opts) {
		try {
			var result = twilioClient.sendSMS({
				to: opts.to,
				body: opts.message
			})
		} catch (err) {
			throw new Meteor.error(err)
		}
		return result
	},
	'insertResponse': function(rest, member, call) {
		try {
			var result = Response.insert({
				time: rest,
				memberId: member,
				callId: call
			})
		} catch (err) {
			throw new Meteor.error(err)
		}
		return result
	},
	'insertCall': function(call) {
		try {
			var result = Calls.insert({
				message: call['message'],
				type: call['type'],
				officer: call['officer']
			})
		} catch (err) {
			throw new Meteor.error(err)
		}
		return result
	},
	'errorLogger': function(error){
		console.log(error)
		return null
	}
})
